# django-cleanmigrations

Bash script for removing all migrations in Django framework

## Notes

This repository is now **working**. However, this repository is still **in development phase**.

## How to run

Make sure the script executable

```bash
chmod +x *.sh
```

Move script(s) to your project root directory

```bash
mv *.sh /path/to/project/dir/
```

Finally, run the script mentioning your apps (without trailing slash) to clean the migrations

```bash
./clean.sh app1 app2 app3 ..
```

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.