#!/usr/bin/env bash

# check args num
if [[ $# -eq 0 ]]; then
    echo
    echo "usage: ${0} <apps>.."
    exit 1
else
    # check if contains slashes
    for i in $@; do
        if [[ ${i} =~ / ]]; then
            # app names contain slash, force exit
            echo
            echo "App names cannot contain slashes! Aborting.."
            exit 1
        fi
    done

    for i in $@; do
        # prompt user
        echo
        echo "Removing migrations in ${i}.."
        echo
        # remove all migration files
        DIR="./${i}/migrations"
        rm -vf ${DIR}/0*.py
    done

    # prompt user when all migrations deleted
    echo
    echo "Done! All migrations deleted!"
    echo
fi
